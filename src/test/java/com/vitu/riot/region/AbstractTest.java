package com.vitu.riot.region;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public abstract class AbstractTest {

    public static final String DATE_TIME_PATTERN_BR = "dd/MM/yyyy hh:mm:ss";
    public static final LocalDateTime DATE_TIME = LocalDateTime.parse("02/06/1997 06:45:15", DateTimeFormatter.ofPattern(DATE_TIME_PATTERN_BR));
    public static final LocalDateTime DATE_TIME_ = LocalDateTime.now();

}
