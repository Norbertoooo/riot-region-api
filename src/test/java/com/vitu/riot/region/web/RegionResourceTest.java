package com.vitu.riot.region.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vitu.riot.region.domain.RegionEnum;
import com.vitu.riot.region.shared.util.DateUtil;
import com.vitu.riot.region.web.dto.ResponseDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class RegionResourceTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    DateUtil dateUtil;

    @Test
    @DisplayName("Should return region name when get with name in lower case")
    void getByName() throws Exception {

        String demaciaName = "demacia";

        ResponseDto responseDto = new ResponseDto(RegionEnum.DEMACIA.name());

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/api/region/" + demaciaName)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(requestBuilder)
                .andExpect(status().is(HttpStatus.OK.value()))
                .andDo(print())
                .andReturn();

        String contentAsString = mvcResult.getResponse().getContentAsString();

        assert contentAsString.equals(new ObjectMapper().writeValueAsString(responseDto));
    }

    @Test
    @DisplayName("Should return region name when get with name in upper case")
    void getByNameInUpperCase() throws Exception {

        String demaciaName = "DEMACIA";

        ResponseDto responseDto = new ResponseDto(RegionEnum.DEMACIA.name());

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/api/region/" + demaciaName)
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder)
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().string(containsString(new ObjectMapper().writeValueAsString(responseDto))))
                .andDo(print());
    }

    @Test
    @DisplayName("Should return error when get region by name that do not exist")
    void shouldReturnConflictWhenGetByName() throws Exception {

        String name = "draven";

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/api/region/" + name)
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder)
                .andExpect(status().is(HttpStatus.CONFLICT.value()))
                .andExpect(jsonPath("$.message").value("Region: draven not found!"))
                .andExpect(jsonPath("$.status").value(409))
                .andExpect(jsonPath("$.path").value("/api/region/draven"))
                .andDo(print());
    }
}