package com.vitu.riot.region;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RiotRegionApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RiotRegionApiApplication.class, args);
	}

}
