package com.vitu.riot.region.shared.util;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static com.vitu.riot.region.shared.Constant.DATE_TIME_PATTERN_BR;

@Component
public class DateUtil {

    public LocalDateTime now() {
        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_TIME_PATTERN_BR));
        return LocalDateTime.parse(now, DateTimeFormatter.ofPattern(DATE_TIME_PATTERN_BR));
    }
}
