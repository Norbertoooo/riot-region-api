package com.vitu.riot.region.domain;

import lombok.Getter;

import java.util.Optional;
import java.util.stream.Stream;

@Getter
public enum RegionEnum {

    DEMACIA(1L),
    NOXUS(2L),
    IONIA(3L),
    SHURIMA(4L),
    AGUAS_DE_SERTINA(5L),
    PILTOVER(6L),
    ZAUN(7L),
    ILHA_DAS_SOMBRAS(8L);

    private final Long id;

    RegionEnum(Long id) {
        this.id = id;
    }

    public static Optional<RegionEnum> getRegionByName(String region) {
        return Stream.of(RegionEnum.values())
                .filter(regionEnum -> regionEnum.name().equalsIgnoreCase(region))
                .findFirst();
    }
}
