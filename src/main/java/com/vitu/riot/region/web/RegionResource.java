package com.vitu.riot.region.web;

import com.vitu.riot.region.domain.RegionEnum;
import com.vitu.riot.region.service.RegionService;
import com.vitu.riot.region.web.dto.ResponseDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/region")
public class RegionResource {

    private final RegionService regionService;

    @GetMapping("/{name}")
    public ResponseEntity<ResponseDto> getByName(@PathVariable String name) {
        RegionEnum regionEnum = regionService.findByName(name);
        return ResponseEntity.ok(new ResponseDto(regionEnum.name()));
    }

}
