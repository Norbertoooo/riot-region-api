package com.vitu.riot.region.exception;

import com.vitu.riot.region.shared.util.DateUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.http.HttpStatus.CONFLICT;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class ControllerAdvice {

    private final DateUtil dateUtil;

    @ExceptionHandler(value = RegionNotFound.class)
    public ResponseEntity<BasicError> regionNotFoundHandler(RegionNotFound regionNotFound, HttpServletRequest request) {
        log.error("{}", regionNotFound.getMessage());

        BasicError basicError = BasicError.builder()
                .status(CONFLICT.value())
                .message(regionNotFound.getMessage())
                .path(request.getRequestURI())
                .dateTime(dateUtil.now())
                .build();

        return ResponseEntity.status(CONFLICT).body(basicError);
    }

}
