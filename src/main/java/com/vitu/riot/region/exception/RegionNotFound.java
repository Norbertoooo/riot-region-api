package com.vitu.riot.region.exception;

public class RegionNotFound extends RuntimeException {

    public RegionNotFound(String region) {
        super("Region: " + region + " not found!");
    }

}
