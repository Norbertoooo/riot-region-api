package com.vitu.riot.region.service;

import com.vitu.riot.region.domain.RegionEnum;
import com.vitu.riot.region.exception.RegionNotFound;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class RegionService {

    public RegionEnum findByName(String name) {
        log.info("Finding region by name: {}", name );
        return RegionEnum.getRegionByName(name)
                .orElseThrow(() -> new RegionNotFound(name));
    }

}
